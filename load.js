var loadState = {
    preload: function () {
        var loadingLabel = game.add.text(game.width/2, 150,
        'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        game.load.image('background', 'assets/background1.png');
        game.load.image('basic_bullet','assets/basic_bullet.png');
        game.load.image('big_bullet','assets/big_bullet.png');
        game.load.spritesheet('player', 'assets/spaceship.png', 39, 45);
        game.load.spritesheet('alien_1','assets/alien_1.png',27,23);
        game.load.spritesheet('alien_2','assets/alien_2.png',29,26);
        game.load.spritesheet('boss','assets/boss.png',399,166);
        game.load.spritesheet('laser','assets/laser_4.png',28,528);
        game.load.image('alien_bullet','assets/alien_bullet.png');
        game.load.image('boss_bullet','assets/boss_bullet.png');
        game.load.image('power','assets/power.png');
        game.load.image('power_out','assets/power_out.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('buff', 'assets/item.png');
        game.load.image('heal', 'assets/heal.png');
        game.load.image('pause','assets/pause.png');
        game.load.image('power_icon','assets/power_icon.png');
        game.load.image('volume','assets/volume.png');
        game.load.image('plus','assets/plus.png');
        game.load.image('minus','assets/minus.png')

        game.load.audio('bullet_sound', 'assets/bullet.mp3');
        game.load.audio('hit_player_sound', 'assets/hit_player.mp3');
        game.load.audio('enemy_die_sound', 'assets/enemy_die.mp3');
        game.load.audio('level_start', 'assets/level_start.mp3');
        game.load.audio('level_clear', 'assets/level_clear.mp3');
        game.load.audio('get_item_sound', 'assets/get_item.mp3');
        game.load.audio('laser_sound','assets/laser_sound.mp3');
        

    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        game.state.start('menu');
    }
}; 