# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : There are three levels in the game, the enemies get stronger and stronger. Player can use energy to fire laser beam at enemy, causing lots of                            damage. Player auto-charges power or get power by killing enemies.
2. Animations : add animations to player, enemies, laser beam...
3. Particle Systems : add particle systems to player and enemies(plays animation when killed).
4. Sound effects : add multiple sound effects(player shooting, player got hit, laser, level start/end....)
5. Leaderboard : Stores players score at the and of the game, and show the top three scores at the menu.

# Bonus Functions Description : 
1. Drop Items: Enemies drop items by random after killed, there are two kinds of items, heal and buff. Heal item heals player by 50Hp, buff item upgrades player's level,               which affects the power of the bullets.
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
