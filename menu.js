var menuState = {
    create: function() {
        game.global.flashTimer = 0;
        // Add a background image
        this.starfield =  game.add.tileSprite(0, 0,400, 540, 'background'); 
        // Display the name of the game
        var nameLabel = game.add.text(game.width/2, 120, 'Raiden',
        { font: '70px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);
        // Show the score at the center of the screen
        var done = false;
        var scoreRef = firebase.database().ref('score_list');
        var total_score = [];
        scoreRef.once('value')
        .then(function (snapshot) {
            console.log(snapshot.val())
            snapshot.forEach(function(childSnapshot) {
                
                var childData = childSnapshot.val();
               
                total_score.push(childData.score);
            })  
            total_score = total_score.sort(function(a,b){
                return b-a;
            }) 
            console.log(total_score[0]);
            var leader = game.add.text(game.width/2,game.height-160,'Leader Board',{font: '25px Arial', fill: '#ffffff'});
            leader.anchor.setTo(0.5,0.5);
            var scoreLabel_1 = game.add.text(game.width/2-40, game.height-120,'1: ' + total_score[0], { font: '25px Arial', fill: '#ffffff' });
            scoreLabel_1.anchor.setTo(0, 0.5);
            var scoreLabel_2 = game.add.text(game.width/2-40, game.height-90,'2: ' + total_score[1], { font: '25px Arial', fill: '#ffffff' });
            scoreLabel_2.anchor.setTo(0, 0.5);
            var scoreLabel_3 = game.add.text(game.width/2-40, game.height-60,'3: ' + total_score[2], { font: '25px Arial', fill: '#ffffff' });
            scoreLabel_3.anchor.setTo(0, 0.5);
            done = true;
            
        }).catch(e => console.log(e.message));
       
        
        // Explain how to start the game

        game.global.startLabel = game.add.text(game.width/2, game.height/2,
        'press the space key to start', { font: '25px Arial', fill: '#ffffff' });
        game.global.startLabel.anchor.setTo(0.5, 0.5);
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        upKey.onDown.add(this.start, this);
        
    },
    update: function(){
        this.starfield.tilePosition.y += 2;
        if(game.time.now>game.global.flashTimer){
            game.global.startLabel.visible = !game.global.startLabel.visible;
            if(game.global.startLabel.visible==true)game.global.flashTimer=game.time.now+600;
            else game.global.flashTimer=game.time.now+300;
        }
    },
    start: function() {
        // Start the actual game
         game.state.start('play');
    },
};