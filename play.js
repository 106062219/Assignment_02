var playState = {
    preload: function() {

    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        this.starfield =  game.add.tileSprite(0, 0,400, 540, 'background'); 
        game.global.score = 0;
        game.global.enemyNum = 10;
        game.global.firingTimer = 0;
        game.global.playerfiringTimer = 0;
        game.global.addenemyTimer = 0;
        game.global.powerTimer = 0;
        game.global.tintTimer = 0;
        game.global.itemtweenTimer = 0;
        game.global.laserTimer = 0;
        game.global.livingEnemies = [];
        game.global.gameLevel = 1;
        game.global.wait = game.time.now + 2600;
        game.global.gameVolume = 0.3;

        game.global.bullet_sound = game.add.audio('bullet_sound');
        game.global.hit_player_sound = game.add.audio('hit_player_sound');
        game.global.enemy_die_sound = game.add.audio('enemy_die_sound');
        game.global.level_start = game.add.audio('level_start');
        game.global.level_clear = game.add.audio('level_clear');
        game.global.get_item_sound = game.add.audio('get_item_sound');
        game.global.laser_sound = game.add.audio('laser_sound');
        
        

        
        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();

        
        this.player = game.add.sprite(game.width/2, game.height*0.75, 'player');
        this.player.health = 100;
        this.player.level = 1;
        this.player.power = 1000;
        this.player.maxpower = 1000;
        this.player.anchor.setTo(0.5,0);
        this.player.animations.add('straight',[0, 1], 8, true);
        this.player.animations.add('right', [2, 3], 4, true);
        this.player.animations.add('left', [4, 5], 4, true);
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;

        
        game.global.power_bar = this.game.add.sprite(160,512,'power');
        game.global.power_bar.scale.setTo((this.player.power/this.player.maxpower), 1);
        var power_out = this.game.add.sprite(160,508,'power_out');
        var power_icon = this.game.add.sprite(140,511,'power_icon');

        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(50, 'basic_bullet');


        this.bigbullets = game.add.group();
        this.bigbullets.enableBody = true;
        this.bigbullets.createMultiple(50, 'big_bullet');
       
        this.laser = game.add.sprite(this.player.x, this.player.y, 'laser');
        this.laser.anchor.setTo(0.5,1);
        this.laser.animations.add('move',[0, 1], 8, true);
        this.laser.animations.add('start',[2,3,4,5], 8, true);
        this.laser.animations.add('stop',[6,7,8,9], 8, true);
        game.physics.arcade.enable(this.laser);
        this.laser.kill();

        this.buffs = game.add.group();
        this.buffs.enableBody = true;
        this.buffs.createMultiple(3, 'buff');

        this.heals = game.add.group();
        this.heals.enableBody = true;
        this.heals.createMultiple(3, 'heal');
        

        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(game.global.enemyNum*game.global.gameLevel, 'alien_1');
        this.enemies.callAll('animations.add', 'animations', 'move', [0, 1], 4, true);
        
        this.enemies1 = game.add.group();
        this.enemies1.enableBody = true;
        this.enemies1.createMultiple(game.global.enemyNum*game.global.gameLevel, 'alien_2');
        this.enemies1.callAll('animations.add', 'animations', 'move', [0, 1], 4, true);
        

        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.createMultiple(30, 'alien_bullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);


       

        

        
        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;

        this.emitter1 = game.add.emitter(422, 320, 15);
        this.emitter1.makeParticles('pixel');
        this.emitter1.setYSpeed(-150, 150);
        this.emitter1.setXSpeed(-150, 150);
        this.emitter1.setScale(2, 0, 2, 0, 800);
        this.emitter1.gravity = 300;

        game.global.pauseLabel =  game.add.text(game.width/2, game.height/2,'Paused' , { font: '40px Arial', fill: '#ffffff' });
        game.global.pauseLabel.anchor.setTo(0.5,0.5);
        game.global.pauseLabel.visible = false;

        game.global.pauseLabel_1 =  game.add.text(game.width/2, game.height/2+40,'Click to Resume' , { font: '20px Arial', fill: '#ffffff' });
        game.global.pauseLabel_1.anchor.setTo(0.5,0.5);
        game.global.pauseLabel_1.visible = false;

        this.pauseButton = this.game.add.sprite(320,10,'pause');
        this.pauseButton.inputEnabled = true;
        this.pauseButton.events.onInputUp.add(function(){ game.paused=true; game.global.pauseLabel.visible=true; game.global.pauseLabel_1.visible=true;},this);
        this.game.input.onDown.add(function(){if(game.paused)game.paused = false;  game.global.pauseLabel.visible=false; game.global.pauseLabel_1.visible=false;},this);

        var mute;
        this.volume = this.game.add.sprite(345,35,'volume');
        this.volume.inputEnabled = true;
        this.volume.events.onInputUp.add(function(){game.sound.mute = !game.sound.mute; mute.visible = game.sound.mute;})
        this.plus = this.game.add.sprite(373,40,'plus');
        this.plus.inputEnabled = true;
        this.plus.events.onInputUp.add(function(){if(game.global.gameVolume<2)game.global.gameVolume+=0.1})
        this.minus = this.game.add.sprite(323,40,'minus');
        this.minus.inputEnabled = true;
        this.minus.events.onInputUp.add(function(){if(game.global.gameVolume>0.1)game.global.gameVolume-=0.1})


        mute = game.add.text(353,27,'/',{ font: '40px Arial', fill: '#ff0000'});
        mute.visible = false;
        game.global.scoreLabel = game.add.text(70, game.height-15,'Score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
        game.global.scoreLabel.anchor.setTo(0.5, 0.5); 
        game.global.playerHealth =  game.add.text(game.width-95, game.height-15,'HP: ' + this.player.health, { font: '25px Arial', fill: '#00ff00' });
        game.global.playerHealth.anchor.setTo(0, 0.5);
        game.global.playerLevel =  game.add.text(game.width-95, game.height-45,'LV: ' + this.player.level, { font: '25px Arial', fill: '#00ff00' });
        game.global.playerLevel.anchor.setTo(0, 0.5); 
        game.global.enemyLeft =  game.add.text(90, 20,'Enemy Left: ' + game.global.enemyNum, { font: '25px Arial', fill: '#ff0000' });
        game.global.enemyLeft.anchor.setTo(0.5, 0.5);
        game.global.endLabel =  game.add.text(game.width/2, game.height/2,'Level 1' , { font: '30px Arial', fill: '#ffffff' });
        game.global.endLabel.anchor.setTo(0.5,0.5)
        game.global.endLabel.visible = true;
        game.global.getitem =  game.add.text(this.player.x, this.player.y,'label' , { font: '15px Arial', fill: '#00ff00' });
        game.global.getitem.anchor.setTo(0.5,0.5)
        game.global.getitem.visible = false;

        game.global.itemtween = game.add.tween(game.global.getitem);
        
        game.global.level_start.play();
    },
  
    update: function() {
        
        game.physics.arcade.collide(this.bullets, this.enemies, this.hit_enemy, null, this);
        game.physics.arcade.collide(this.bigbullets, this.enemies, this.hit_enemy_big, null, this);
        game.physics.arcade.collide(this.bullets, this.enemies1, this.hit_enemy, null, this);
        game.physics.arcade.collide(this.bigbullets, this.enemies1, this.hit_enemy_big, null, this);

        game.physics.arcade.overlap(this.laser, this.enemies, this.hit_enemy_laser, null, this);
        game.physics.arcade.overlap(this.laser, this.enemies1, this.hit_enemy_laser, null, this);
        

        game.physics.arcade.collide(this.player, this.enemies, this.hit_player, null, this);
        game.physics.arcade.collide(this.player, this.enemies1, this.hit_player, null, this);
        game.physics.arcade.collide(this.player, this.enemyBullets, this.get_shot, null, this);
        game.physics.arcade.collide(this.player, this.buffs, this.get_buff, null, this);
        game.physics.arcade.collide(this.player, this.heals, this.get_heal, null, this);
        this.starfield.tilePosition.y += 2;
        if (game.time.now > game.global.firingTimer)
        {
            this.addAlienBullets();
        }

        var sKey = game.input.keyboard.addKey(Phaser.Keyboard.SHIFT);
        if(this.player.power>0&&this.player.health>0&&game.global.enemyNum>0){
            sKey.onDown.add(this.fire_laser, this);
        }
        sKey.onUp.add(this.stop_laser, this);
        
        if(game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)&&this.player.power>0&&this.player.health>0){
            console.log(this.player.power);
            this.move_laser();
            if(this.player.power>8)this.player.power-=8;  
            else this.player.power = 0;           
        }
        if(this.player.power<1000&&!game.input.keyboard.isDown(Phaser.Keyboard.SHIFT))this.player.power+=0.5;
        if(this.player.power==0){
            this.stop_laser();
        }
        

        if((!game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)||this.player.power==0)&&game.global.enemyNum != 0&&game.time.now > game.global.wait&&game.time.now > game.global.playerfiringTimer&&this.player.health>0)
        {
            game.global.bullet_sound.stop();
            game.global.bullet_sound.play();
            if(this.player.level==1)this.addlv1Bullets();
            if(this.player.level==2)this.addlv2Bullets();
            if(this.player.level==3)this.addlv3Bullets();
            if(this.player.level==4)this.addlv4Bullets();
            if(this.player.level==5)this.addlv5Bullets();
        }
        if(game.time.now > game.global.wait&&game.time.now>game.global.addenemyTimer&&game.global.enemyNum>0)
        {
            game.global.endLabel.visible=false;
            this.addEnemies();
        }
        if(game.time.now > game.global.itemtweenTimer)
        {
            game.global.getitem.visible = false;
        }

        if (game.global.enemyNum <= 0)
        {
            if(game.global.gameLevel<4){
                
                game.global.endLabel.text = "       Level Clear \n\n Space to Next Level";
                game.global.endLabel.visible = true;
                //the "click to restart" handler
                this.enemies.callAll('kill');
                this.enemies1.callAll('kill');
                this.enemyBullets.callAll('kill');
                var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
                upKey.onDown.add(this.restart, this);
            }
            else if(game.global.gameLevel==4){
        
                game.global.endLabel.text = "  Congratulations, You Won!! \n\n           Score: "+game.global.score+"\n          Space to Menu";
                game.global.endLabel.visible = true;
                //the "click to restart" handler
                this.enemies.callAll('kill');
                this.enemies1.callAll('kill');
                this.enemyBullets.callAll('kill');
                var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
                upKey.onDown.add(this.playerDie, this);
            }
        }
        if (!this.player.inWorld) { this.player.health=0;}
        if(this.player.health<=0)
        {
            game.global.gameLevel = 1;
            game.global.endLabel.text = "   Game Over!! \n\n     Score: "+game.global.score+"\n\n  Space to Menu";
            game.global.endLabel.visible = true;
            //the "click to restart" handler
            var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            upKey.onDown.add(this.playerDie, this);
            this.blockParticle(this.player);
            this.player.kill();
        }
        
        
        
        this.movePlayer();
        this.enemies.callAll('play', 0, 'move');
        this.enemies1.callAll('play', 0, 'move');
        if(game.time.now>game.global.tintTimer){
            this.enemies.setAll('tint',0xffffff);
            this.enemies1.setAll('tint',0xffffff);
        }
        game.global.bullet_sound.volume = game.global.gameVolume*0.5;
        game.global.hit_player_sound.volume = game.global.gameVolume;
        game.global.enemy_die_sound.volume = game.global.gameVolume; 
        game.global.level_start.volume = game.global.gameVolume;
        game.global.level_clear.volume = game.global.gameVolume; 
        game.global.get_item_sound.volume = game.global.gameVolume; 
        game.global.laser_sound.volume = game.global.gameVolume;

        game.global.power_bar.scale.setTo((this.player.power/this.player.maxpower),1);       
        game.global.scoreLabel.text = 'Score: ' + game.global.score; 
        game.global.playerHealth.text = 'HP: '+ this.player.health;
        game.global.enemyLeft.text = 'Enemy Left: '+ game.global.enemyNum;
        game.global.playerLevel.text = 'LV: '+this.player.level;
    }, 


///////////bullet hit enemy///////////////////////////////////////////
    hit_enemy: function(bullet, enemy){
        enemy.tint = 0xff0000;
        game.global.tintTimer = game.time.now+23;
        var original_health = enemy.health;
        enemy.health=original_health-10;
        //console.log(enemy.health);
        if(enemy.health<=0) {
            var itemtween = game.add.tween(game.global.getitem);
            game.global.getitem.visible = true;
            game.global.getitem.text = '+50power'
            game.global.getitem.x = this.player.x;
            game.global.getitem.y = this.player.y;
            itemtween.to({y:this.player.y-20},200).start();
            game.global.itemtweenTimer = game.time.now+400;

            game.camera.shake(0.01,200);
            game.global.enemy_die_sound.stop();
            game.global.enemy_die_sound.play();

            if(this.player.power<=950) this.player.power+=100;
            else this.player.power=1000;

            enemy.kill();
            enemy.destroy();
            game.global.enemyNum -= 1;
            this.blockParticle(enemy);
        
            game.global.score = game.global.score+10*game.global.gameLevel;
            if(game.rnd.between(0, 100)>70&&game.rnd.between(0, 100)<85&&this.player.level<5){
                this.dropBuff(enemy);
            }
            else if(game.rnd.between(0, 100)>=85){
                this.dropHeal(enemy);
            }
        }
        bullet.kill(); 
        if(game.global.enemyNum<=0){
            game.global.level_clear.play();
            game.global.score +=100*game.global.gameLevel;
            game.global.gameLevel += 1;
        }
    },
    hit_enemy_big: function(bullet, enemy){
        enemy.tint = 0xff0000;
        game.global.tintTimer = game.time.now+23;
        var original_health = enemy.health;
        enemy.health=original_health-30;
        //console.log(enemy.health);
        if(enemy.health<=0) {
            var itemtween = game.add.tween(game.global.getitem);
            game.global.getitem.visible = true;
            game.global.getitem.text = '+50power'
            game.global.getitem.x = this.player.x;
            game.global.getitem.y = this.player.y;
            itemtween.to({y:this.player.y-20},200).start();
            game.global.itemtweenTimer = game.time.now+400;

            game.camera.shake(0.01,200);
            game.global.enemy_die_sound.stop();
            game.global.enemy_die_sound.play();

            if(this.player.power<=950) this.player.power+=100;
            else this.player.power=1000;

            enemy.kill();
            enemy.destroy();
            game.global.enemyNum -= 1;
            this.blockParticle(enemy);
            game.global.score =game.global.score+10*game.global.gameLevel;
            if(game.rnd.between(0, 100)>70&&game.rnd.between(0, 100)<85&&this.player.level<5){
                this.dropBuff(enemy);
            }
            else if(game.rnd.between(0, 100)>=85){
                this.dropHeal(enemy);
            }
        }
        bullet.kill();
        if(game.global.enemyNum<=0){
            game.global.level_clear.play();
            game.global.score +=100*game.global.gameLevel;
            game.global.gameLevel += 1;
        }
    },

    hit_enemy_laser: function(laser, enemy){
        enemy.tint = 0xff0000;
        game.global.tintTimer = game.time.now+23;
        var original_health = enemy.health;
        enemy.health=original_health-10;
        //console.log(enemy.health);
        if(enemy.health<=0) {
            var itemtween = game.add.tween(game.global.getitem);
            game.global.getitem.visible = true;
            game.global.getitem.text = '+50power'
            game.global.getitem.x = this.player.x;
            game.global.getitem.y = this.player.y;
            itemtween.to({y:this.player.y-20},200).start();
            game.global.itemtweenTimer = game.time.now+400;

            game.camera.shake(0.01,200);
            game.global.enemy_die_sound.stop();
            game.global.enemy_die_sound.play();

            if(this.player.power<=950) this.player.power+=100;
            else this.player.power=1000;

            enemy.kill();
            enemy.destroy();
            game.global.enemyNum -= 1;
            this.blockParticle(enemy);
            game.global.score = game.global.score+10*game.global.gameLevel;
            if(game.rnd.between(0, 100)>70&&game.rnd.between(0, 100)<85&&this.player.level<5){
                this.dropBuff(enemy);
            }
            else if(game.rnd.between(0, 100)>=85){
                this.dropHeal(enemy);
            }
        }
        if(game.global.enemyNum<=0){
            game.global.level_clear.play();
            game.global.score +=100*game.global.gameLevel;
            game.global.gameLevel += 1;
        }
    },
    
    dropBuff: function(enemy){
        var item = this.buffs.getFirstDead();
        if (!item) { return;}
        item.anchor.setTo(0.5, 0.5);
        item.reset(enemy.x, enemy.y);
        item.body.velocity.y=100;
        item.checkWorldBounds = true;
        item.outOfBoundsKill = true;
    },

    dropHeal: function(enemy){
        var item = this.heals.getFirstDead();
        if (!item) { return;}
        item.anchor.setTo(0.5, 0.5);
        item.reset(enemy.x, enemy.y);
        item.body.velocity.y=100;
        item.checkWorldBounds = true;
        item.outOfBoundsKill = true;
    },
//////////////////////////////////////////////////////////

//////player collide///////////////////////////////////////
    hit_player: function(player, enemy){
        var original_hp = this.player.health;
        this.player.health = original_hp-20;
        enemy.kill();
        this.blockParticle(enemy);
        game.global.score += 5;
        game.global.enemyNum -= 1;
        if(game.global.enemyNum<=0){
            game.global.level_clear.play();
            game.global.score +=100*game.global.gameLevel;
            game.global.gameLevel += 1;
        }
        game.global.hit_player_sound.stop();
        game.global.hit_player_sound.play();
        game.camera.shake(0.01,200);
        game.camera.flash(0xff0000, 300);
    },

    get_shot: function(player,enemyBullet){
        enemyBullet.kill();
        var original_hp = this.player.health;
        this.player.health = original_hp-10;
        game.global.hit_player_sound.stop();
        game.global.hit_player_sound.play();
        game.camera.shake(0.01,200);
        game.camera.flash(0xff0000, 300);
    },

    get_buff: function(player,item){
        game.global.get_item_sound.stop();
        game.global.get_item_sound.play();
        var itemtween = game.add.tween(game.global.getitem);
        game.global.getitem.visible = true;
        game.global.getitem.text = 'level up'
        game.global.getitem.x = this.player.x;
        game.global.getitem.y = this.player.y;
        itemtween.to({y:this.player.y-20},200).start();
        game.global.itemtweenTimer = game.time.now+400;
      
        //console.log(this.player.level);
        item.kill();
        var original_lv = this.player.level;
        if(this.player.level<5){
            this.player.level = original_lv+1;
        }
        game.global.score += 50;
    },

    get_heal: function(player,item){
        game.global.get_item_sound.stop();
        game.global.get_item_sound.play();
        var itemtween = game.add.tween(game.global.getitem);
        game.global.getitem.visible = true;
        game.global.getitem.text = '+30HP'
        game.global.getitem.x = this.player.x;
        game.global.getitem.y = this.player.y;
        itemtween.to({y:this.player.y-20},200).start();
        game.global.itemtweenTimer = game.time.now+400;

       
        item.kill();
        var original_hp = this.player.health;
        if(this.player.health>70){
            this.player.health=100;
        }
        else{
            this.player.health = original_hp+30;
        }
        game.global.score += 50;

    },

///////////////////////////////////////////////////////////

    blockParticle: function(enemy) {
        this.emitter.x=enemy.x;
        this.emitter.y=enemy.y;
        this.emitter.start(true, 800, null, 15);
    },
    blockParticleSmall: function(bullet) {
        this.emitter1.x=bullet.x;
        this.emitter1.y=bullet.y;
        this.emitter1.start(true, 800, null, 15);
    },

//////bullet levels /////////////////////////////////////
    addlv1Bullets: function(){
        var bullet = this.bullets.getFirstDead();
        if (!bullet) { return;}
        bullet.anchor.setTo(0.5, 0.5);
        bullet.reset(this.player.x, this.player.y);
        bullet.body.velocity.y=-250;
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;
        game.global.playerfiringTimer = game.time.now + 150;
    },

    addlv2Bullets: function(){
        var bullet = this.bullets.getFirstDead();
        if (!bullet) { return;}
        bullet.anchor.setTo(0.5, 0.5);
        bullet.reset(this.player.x+5, this.player.y);
        bullet.body.velocity.y=-250;
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;

        var bullet2 = this.bullets.getFirstDead();
        if (!bullet2) { return;}
        bullet2.anchor.setTo(0.5, 0.5);
        bullet2.reset(this.player.x-5, this.player.y);
        bullet2.body.velocity.y=-250;
        bullet2.checkWorldBounds = true;
        bullet2.outOfBoundsKill = true;

        game.global.playerfiringTimer = game.time.now + 150;
    },

    addlv3Bullets: function(){
        var bullet = this.bigbullets.getFirstDead();
        if (!bullet) { return;}
        bullet.anchor.setTo(0.5, 0.5);
        bullet.reset(this.player.x, this.player.y);
        bullet.body.velocity.y=-250;
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;

        game.global.playerfiringTimer = game.time.now + 150;
    },

    addlv4Bullets: function(){
        var bullet = this.bigbullets.getFirstDead();
        if (!bullet) { return;}
        bullet.anchor.setTo(0.5, 0.5);
        bullet.reset(this.player.x, this.player.y);
        bullet.body.velocity.y=-250;
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;

        var bullet2 = this.bullets.getFirstDead();
        if (!bullet2) { return;}
        bullet2.anchor.setTo(0.5, 0.5);
        bullet2.reset(this.player.x-8, this.player.y);
        bullet2.body.velocity.y=-250;
        bullet2.checkWorldBounds = true;
        bullet2.outOfBoundsKill = true;

        var bullet3 = this.bullets.getFirstDead();
        if (!bullet2) { return;}
        bullet3.anchor.setTo(0.5, 0.5);
        bullet3.reset(this.player.x+8, this.player.y);
        bullet3.body.velocity.y=-250;
        bullet3.checkWorldBounds = true;
        bullet3.outOfBoundsKill = true;

        game.global.playerfiringTimer = game.time.now + 150;
    },

    addlv5Bullets: function(){
        var bullet = this.bigbullets.getFirstDead();
        if (!bullet) { return;}
        bullet.anchor.setTo(0.5, 0.5);
        bullet.reset(this.player.x-10, this.player.y);
        bullet.body.velocity.y=-250;
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;

        var bullet1 = this.bigbullets.getFirstDead();
        if (!bullet1) { return;}
        bullet1.anchor.setTo(0.5, 0.5);
        bullet1.reset(this.player.x+10, this.player.y);
        bullet1.body.velocity.y=-250;
        bullet1.checkWorldBounds = true;
        bullet1.outOfBoundsKill = true;

        game.global.playerfiringTimer = game.time.now + 150;
    },

    fire_laser: function(){
       game.global.laserTimer = game.time.now+500;
       this.laser.animations.stop();
       this.laser.animations.play('start');
       game.global.laser_sound.loop = true;
       game.global.laser_sound.play();
       this.laser.revive();
    },
    stop_laser: function(){
        game.global.laser_sound.stop();
        this.laser.kill();
    },
    move_laser: function(){
        this.laser.x = this.player.x;
        this.laser.y = this.player.y;
        if(game.time.now>game.global.laserTimer)this.laser.animations.play('move');
    },
//////////////////////////////////////////////////////////////////////////

/////////////create enemy///////////////////////////////////////////////////
    addAlienBullets: function(enemies){
        var enemyBullet = this.enemyBullets.getFirstExists(false);
        game.global.livingEnemies.length=0;

        this.enemies.forEachAlive(function(enemy){

            // put every living enemy in an array
            game.global.livingEnemies.push(enemy);
        });


        if (enemyBullet && game.global.livingEnemies.length > 0)
        {
            
            var random=game.rnd.integerInRange(0,game.global.livingEnemies.length-1);

            // randomly select one of them
            var shooter=game.global.livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);

            game.physics.arcade.moveToObject(enemyBullet,this.player,120+(game.global.gameLevel-1)*20);
            game.global.firingTimer = game.time.now + 2000;
        }
    },
    addEnemies: function(){
        //console.log('hi');
        game.global.addenemyTimer = (game.time.now + 1700 + game.rnd.between(0, 1000))-(game.global.gameLevel-1)*100;
        var enemy;
        var flag;
        if(game.rnd.between(0,100)>50){
            enemy = this.enemies.getFirstExists(false);
            flag=0;
            if (!enemy) { return;}
        }
        else{
            enemy = this.enemies1.getFirstExists(false);
            flag=1;  
            if (!enemy) { return;}
        }
        //console.log(enemy.health);
        enemy.anchor.setTo(0.5, 0.5);
        enemy.reset(game.rnd.between(20, 380), 0);
        enemy.body.velocity.y = 10+game.global.gameLevel*10+game.rnd.between(-15,15);
        enemy.body.immovable = true;
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
        if(flag==0)enemy.health=100+(game.global.gameLevel-1+(this.player.level-1)/2)*100;
        else enemy.health=200+(game.global.gameLevel-1+(this.player.level-1)/2)*150;
        

    },
///////////////////////////////////////////////////////////////////////
    restart: function(){
        if(game.global.enemyNum!=0)return;
        game.global.level_start.play();
        game.global.enemyNum = 10+(game.global.gameLevel-1)*5;
        game.global.endLabel.text = 'LEVEL '+ game.global.gameLevel;
        this.player.level = 1;
        this.player.power = 1000;
        game.global.wait = game.time.now + 2600;
        
        this.enemies.createMultiple(game.global.enemyNum*game.global.gameLevel, 'alien_1');
        this.enemies.callAll('animations.add', 'animations', 'move', [0, 1], 4, true);
        this.enemies1.createMultiple(game.global.enemyNum*game.global.gameLevel, 'alien_2');
        this.enemies1.callAll('animations.add', 'animations', 'move', [0, 1], 4, true);
    },
    playerDie: function() { 
        var scoreListRef = firebase.database().ref('score_list');
                var newPostRef = scoreListRef.push();
                newPostRef.set({
                    // ...
                    "score": game.global.score
                });
        game.state.start('menu');
    },

   
    movePlayer: function() {
        if(this.cursor.left.isDown&&this.cursor.up.isDown){
            this.player.body.velocity.x = -200;
            this.player.body.velocity.y = -200;
            this.player.animations.play('left');
        }
        else if(this.cursor.left.isDown&&this.cursor.down.isDown){
            this.player.body.velocity.x = -200;
            this.player.body.velocity.y = 200;
            this.player.animations.play('left');
        }
        else if(this.cursor.right.isDown&&this.cursor.up.isDown){
            this.player.body.velocity.x = 200;
            this.player.body.velocity.y = -200;
            this.player.animations.play('right');
        }
        else if(this.cursor.right.isDown&&this.cursor.down.isDown){
            this.player.body.velocity.x = 200;
            this.player.body.velocity.y = 200;
            this.player.animations.play('right');
        }
        else if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.player.animations.play('left');
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.player.animations.play('right');
        }    

        else if (this.cursor.up.isDown) { 
            if(this.cursor.left.isDown) this.player.animations.play('left');   
            else if(this.cursor.right.isDown)this.player.animations.play('right');
            else if(!this.cursor.left.isDown&&!this.cursor.right.isDown) this.player.animations.play('straight');
            this.player.body.velocity.y = -200;
            
        }  
        else if (this.cursor.down.isDown) { 
            if(this.cursor.left.isDown) this.player.animations.play('left');   
            else if(this.cursor.right.isDown)this.player.animations.play('right');
            else if(!this.cursor.left.isDown&&!this.cursor.right.isDown) this.player.animations.play('straight');
            this.player.body.velocity.y = 200;
            
        }
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;

            this.player.animations.play('straight');
            
        } 
    }
};





